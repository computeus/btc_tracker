class ChartDatum < ActiveRecord::Base
  def self.parse_chart
    start_date = ChartDatum.last.created_at

    prices = Price.where(created_at: start_date..(start_date + 15.minutes))

    if prices.present?
      volume = prices.last.volume - prices.first.volume

      volume = 0 if volume < 0

      chart_datum = ChartDatum.create(
        high:   prices.order(last_order: :DESC).first.last_order,
        low:    prices.order(last_order: :ASC).first.last_order,
        open:   prices.first.last_order,
        close:  prices.last.last_order,
        volume: volume
      )
    end
  end

  def one_day_ago
    ChartDatum.where(created_at: (created_at - 1.minute)..(created_at + 1.minute)).first
  end
end
