require 'open-uri'
class Price < ActiveRecord::Base
  def self.parse_ticker
    self.delay(run_at: Time.now + 9.seconds).parse_ticker

    ticker_data = JSON.load(open('https://koinim.com/ticker'))

    self.create(
      sell:        ticker_data["sell"],
      high:        ticker_data["high"],
      buy:         ticker_data["buy"],
      change_rate: ticker_data["change_rate"],
      bid:         ticker_data["bid"],
      wavg:        ticker_data["wavg"],
      last_order:  ticker_data["last_order"],
      volume:      ticker_data["volume"],
      low:         ticker_data["low"],
      ask:         ticker_data["ask"],
      avg:         ticker_data["avg"]
    )
  end
end
