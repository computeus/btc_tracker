class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.decimal :sell, scale: 8, precision: 12
      t.decimal :high, scale: 8, precision: 12
      t.decimal :buy, scale: 8, precision: 12
      t.decimal :change_rate, scale: 8, precision: 12
      t.decimal :bid, scale: 8, precision: 12
      t.decimal :wavg, scale: 8, precision: 12
      t.decimal :last_order, scale: 8, precision: 12
      t.decimal :volume, scale: 8, precision: 12
      t.decimal :low, scale: 8, precision: 12
      t.decimal :ask, scale: 8, precision: 12
      t.decimal :avg, scale: 8, precision: 12

      t.timestamps null: false
    end
  end
end
