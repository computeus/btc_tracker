class CreateChartData < ActiveRecord::Migration
  def change
    create_table :chart_data do |t|
      t.decimal :open, scale: 8, precision: 12
      t.decimal :close, scale: 8, precision: 12
      t.decimal :high, scale: 8, precision: 12
      t.decimal :low, scale: 8, precision: 12
      t.decimal :volume, scale: 8, precision: 12

      t.timestamps null: false
    end
    add_index :chart_data, :open
    add_index :chart_data, :close
    add_index :chart_data, :high
    add_index :chart_data, :low
    add_index :chart_data, :volume
  end
end
