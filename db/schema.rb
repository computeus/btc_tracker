# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161024223123) do

  create_table "chart_data", force: :cascade do |t|
    t.decimal  "open",       precision: 12, scale: 8
    t.decimal  "close",      precision: 12, scale: 8
    t.decimal  "high",       precision: 12, scale: 8
    t.decimal  "low",        precision: 12, scale: 8
    t.decimal  "volume",     precision: 12, scale: 8
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "chart_data", ["close"], name: "index_chart_data_on_close", using: :btree
  add_index "chart_data", ["high"], name: "index_chart_data_on_high", using: :btree
  add_index "chart_data", ["low"], name: "index_chart_data_on_low", using: :btree
  add_index "chart_data", ["open"], name: "index_chart_data_on_open", using: :btree
  add_index "chart_data", ["volume"], name: "index_chart_data_on_volume", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "prices", force: :cascade do |t|
    t.decimal  "sell",        precision: 12, scale: 8
    t.decimal  "high",        precision: 12, scale: 8
    t.decimal  "buy",         precision: 12, scale: 8
    t.decimal  "change_rate", precision: 12, scale: 8
    t.decimal  "bid",         precision: 12, scale: 8
    t.decimal  "wavg",        precision: 12, scale: 8
    t.decimal  "last_order",  precision: 12, scale: 8
    t.decimal  "volume",      precision: 12, scale: 8
    t.decimal  "low",         precision: 12, scale: 8
    t.decimal  "ask",         precision: 12, scale: 8
    t.decimal  "avg",         precision: 12, scale: 8
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

end
