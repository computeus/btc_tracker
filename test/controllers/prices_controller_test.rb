require 'test_helper'

class PricesControllerTest < ActionController::TestCase
  setup do
    @price = prices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:prices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create price" do
    assert_difference('Price.count') do
      post :create, price: { ask: @price.ask, avg: @price.avg, bid: @price.bid, buy: @price.buy, change_rate: @price.change_rate, high: @price.high, last_order: @price.last_order, low: @price.low, sell: @price.sell, volume: @price.volume, wavg: @price.wavg }
    end

    assert_redirected_to price_path(assigns(:price))
  end

  test "should show price" do
    get :show, id: @price
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @price
    assert_response :success
  end

  test "should update price" do
    patch :update, id: @price, price: { ask: @price.ask, avg: @price.avg, bid: @price.bid, buy: @price.buy, change_rate: @price.change_rate, high: @price.high, last_order: @price.last_order, low: @price.low, sell: @price.sell, volume: @price.volume, wavg: @price.wavg }
    assert_redirected_to price_path(assigns(:price))
  end

  test "should destroy price" do
    assert_difference('Price.count', -1) do
      delete :destroy, id: @price
    end

    assert_redirected_to prices_path
  end
end
